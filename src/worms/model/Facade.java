package worms.model;

public class Facade implements IFacade {

	public Facade() {
		
	}
	
	public Worm createWorm(double x, double y, double direction, double radius,
			String name) {
		try {
			return new Worm(x, y, direction, radius, name);
		} 
		catch (IllegalArgumentException exc) {
			throw new ModelException(exc);
		}
	};

	/**
	 * Returns whether or not the given worm can move a given number of steps.
	 */
	public boolean canMove(Worm worm, int nbSteps) {
		try {
			return worm.canMove(nbSteps);
		} 
		catch (IllegalArgumentException exc) {
			throw new ModelException(exc);
		}
	};

	/**
	 * Moves the given worm by the given number of steps.
	 */
	public void move(Worm worm, int nbSteps) {
		try {
			worm.move(nbSteps);
		} catch (IllegalArgumentException exc) {
			throw new ModelException(exc);
		}
	};

	/**
	 * Returns whether or not the given worm can turn by the given angle.
	 */
	public boolean canTurn(Worm worm, double angle) {
		return worm.canTurn(angle);
	};

	/**
	 * Turns the given worm by the given angle.
	 */
	public void turn(Worm worm, double angle) {
		worm.turn(angle);
	};

	/**
	 * Makes the given worm jump.
	 */
	public void jump(Worm worm) {
		try {
			worm.jump();
		}
		catch (UnsupportedOperationException exc) {
			throw new ModelException(exc);
		}	
	};

	/**
	 * Returns the total amount of time (in seconds) that a
	 * jump of the given worm would take.
	 */
	public double getJumpTime(Worm worm) {
		try {
			return worm.getJumpTime();
		} 
		catch (ArithmeticException exc) {
			throw new ModelException(exc);
		}
		catch(UnsupportedOperationException exc) {
			throw new ModelException(exc);
		}
	};

	/**
	 * Returns the location on the jump trajectory of the given worm
	 * after a time t.
	 *  
	 * @return An array with two elements,
	 *  with the first element being the x-coordinate and
	 *  the second element the y-coordinate
	 */
	public double[] getJumpStep(Worm worm, double t) {
		try {
			return worm.getJumpStep(t);
		} 
		catch(IllegalArgumentException exc) {
			throw new ModelException(exc);
		}
	};

	/**
	 * Returns the x-coordinate of the current location of the given worm.
	 */
	public double getX(Worm worm) {
		return worm.getX();
	};

	/**
	 * Returns the y-coordinate of the current location of the given worm.
	 */
	public double getY(Worm worm) {
		return worm.getY();
	};

	/**
	 * Returns the current orientation of the given worm (in radians).
	 */
	public double getOrientation(Worm worm) {
		return worm.getDirection();
	};

	/**
	 * Returns the radius of the given worm.
	 */
	public double getRadius(Worm worm) {
		return worm.getRadius();
	};
	
	/**
	 * Sets the radius of the given worm to the given value.
	 */
	public void setRadius(Worm worm, double newRadius) {
		try {
			worm.setSizeAspects(newRadius);
		}
		catch (IllegalArgumentException exc) {
			throw new ModelException(exc);
		}
	};
	
	/**
	 * Returns the minimal radius of the given worm.
	 */
	public double getMinimalRadius(Worm worm) {
		return worm.getMinimumRadius();
	};

	/**
	 * Returns the current number of action points of the given worm.
	 */
	public int getActionPoints(Worm worm) {
		return worm.getActionPoints();
	};
	
	/**
	 * Returns the maximum number of action points of the given worm.
	 */
	public int getMaxActionPoints(Worm worm) {
		return worm.getMaximumActionPoints();
	};
	
	/**
	 * Returns the name the given worm.
	 */
	public String getName(Worm worm) {
		return worm.getName();
	};

	/**
	 * Renames the given worm.
	 */
	public void rename(Worm worm, String newName) {
		try {
			worm.setName(newName);
		} catch (IllegalArgumentException exc) {
			throw new ModelException(exc);
		}
	};

	/**
	 * Returns the mass of the given worm.
	 */
	public double getMass(Worm worm) {
		return worm.getMass();
	};
	
}
