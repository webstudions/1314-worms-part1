package worms.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import worms.util.Util;

public class WormsTest {
	
	private static final double EPS = Util.DEFAULT_EPSILON;
	private static Worm theWorm;
	
	@Before
	public void setUp() throws Exception {
		theWorm = new Worm(50, 50, 0.2, 0.5, "TheWorm");
	}
	
	@Test
	public void testConstructor_NotNull() {
		Worm theWorm = new Worm(1, 2, 3, 4, "TheWorm");
		assertNotNull(theWorm);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testConstructor_TooSmallRadius() {
		new Worm(50, 50, 0.2, 0.1, "TheWorm");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testConstructor_WrongName() {
		new Worm(50, 50, 0.2, 0.5, "theWorm");
	}
	
	@Test
	public void testSetX() {
		theWorm.setX(100);
		assertEquals(100, theWorm.getX(), EPS);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testSetX_IsNaN() {
		theWorm.setX(Double.NaN);
	}
	
	@Test
	public void testGetX() {
		assertEquals(50, theWorm.getX(), EPS);
	}
	
	@Test
	public void testSetY() {
		theWorm.setY(100);
		assertEquals(100, theWorm.getY(), EPS);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testSetY_IsNaN() {
		theWorm.setY(Double.NaN);
	}
	
	@Test
	public void testGetY() {
		assertEquals(50, theWorm.getY(), EPS);
	}
	
	@Test
	public void testSetDirection_SingleCase() {
		theWorm.setDirection(0.7);
		assertEquals(0.7, theWorm.getDirection(), EPS);
	}
	
	@Test
	public void testGetDirection() {
		assertEquals(0.2, theWorm.getDirection(), EPS);
	}
	
	@Test
	public void testSetSizeAspects_SingleCase() {
		theWorm.setSizeAspects(0.75);
		assertEquals(0.75, theWorm.getRadius(), EPS);
		assertEquals(1876.708911,theWorm.getMass(), EPS);
		assertEquals(1877,theWorm.getMaximumActionPoints());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSetSizeAspects_RadiusTooSmall() {
		theWorm.setSizeAspects(0.2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSetSizeAspects_NaN() {
		theWorm.setSizeAspects(Double.NaN);
	}
	
	@Test
	public void testGetRadius() {
		assertEquals(0.5,theWorm.getRadius(), EPS);
	}
	
	@Test
	public void testSetName_SingleCase() {
		theWorm.setName("Stijn's \"friends\"");
		assertEquals("Stijn's \"friends\"", theWorm.getName());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSetName_smallStart() {
		theWorm.setName("stijn");
	}
	
	@Test
	public void testGetName() {
		assertEquals("TheWorm", theWorm.getName());
	}
	
	@Test
	public void testSetMass_SingleCase() {
		assertEquals(556.0618997,theWorm.getMass(), EPS);
	}
	
	@Test
	public void testGetMass() {
		assertEquals(556.0618997, theWorm.getMass(), EPS);
	}
	
	@Test
	public void testSetMaximumActionPoints_SingleCase() {
		assertEquals(556, theWorm.getMaximumActionPoints());
	}
	
	@Test
	public void testGetMaximumActionPoints() {
		assertEquals(556, theWorm.getMaximumActionPoints());
	}
	
	@Test
	public void testSetActionPoints_SingleCase() {
		theWorm.setActionPoints(250);
		assertEquals(250, theWorm.getActionPoints());
	}
	
	@Test
	public void testSetActionPoints_TooLarge() {
		theWorm.setActionPoints(1000);
		assertEquals(theWorm.getMaximumActionPoints(), theWorm.getActionPoints());
	}
	
	@Test
	public void testSetActionPoints_TooSmall() {
		theWorm.setActionPoints(-10);
		assertEquals(0, theWorm.getActionPoints());
	}
	
	@Test
	public void testSetActionPoints_UpperBound() {
		theWorm.setActionPoints(theWorm.getMaximumActionPoints());
		assertEquals(theWorm.getMaximumActionPoints(), theWorm.getActionPoints());
	}
	
	@Test
	public void testSetActionPoints_LowerBound() {
		theWorm.setActionPoints(0);
		assertEquals(0, theWorm.getActionPoints());
	}

	@Test
	public void testIsValidRadius_TrueCase() {
		assertTrue(theWorm.isValidRadius(0.75));
	}
	
	@Test
	public void testIsValidRadius_LowerBound() {
		assertTrue(theWorm.isValidRadius(theWorm.getMinimumRadius()));
	}
	
	@Test
	public void testIsValidRadius_IsNaN() {
		assertFalse(theWorm.isValidRadius(Double.NaN));
	}
	
	@Test
	public void testIsValidRadius_TooSmall() {
		assertFalse(theWorm.isValidRadius(0.1));
	}
	
	@Test
	public void testIsValidDirection_TrueCase() {
		assertTrue(theWorm.isValidDirection(1));
	}
	
	@Test
	public void testIsValidDirection_LowerBound() {
		assertTrue(theWorm.isValidDirection(0));
	}
	
	@Test
	public void testIsValidDirection_UpperBound() {
		assertTrue(theWorm.isValidDirection(2*Math.PI));
	}
	
	@Test
	public void testIsValidDirection_TooSmall() {
		assertFalse(theWorm.isValidDirection(-0.5));
	}
	
	@Test
	public void testIsValidDirection_TooLarge() {
		assertFalse(theWorm.isValidDirection(7));
	}
	
	@Test
	public void testIsValidName_TrueCase() {
		assertTrue(theWorm.isValidName("Stijn's \"FrieNds\""));
	}
	
	@Test
	public void testIsValidName_SmallFirst() {
		assertFalse(theWorm.isValidName("stijn"));
	}
	
	@Test
	public void testIsValidName_ExclMark() {
		assertFalse(theWorm.isValidName("Stijn!"));
	}
	
	@Test
	public void testIsValidName_Number() {
		assertFalse(theWorm.isValidName("Stijn007"));
	}
	
	@Test
	public void testIsValidName_TooShort() {
		assertFalse(theWorm.isValidName("S"));
	}
	
	@Test
	public void testIsValidName_CharFirst() {
		assertFalse(theWorm.isValidName("'stijn'"));
	}
	
	@Test
	public void testCanMove_TrueCase() {
		assertTrue(theWorm.canMove(10));
	}
	
	@Test
	public void testCanMove_TooFar() {
		assertFalse(theWorm.canMove(10000));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testCanMove_NegativeSteps() {
		theWorm.canMove(-10);
	}
	
	@Test
	public void testCanMove_LowerBound() {
		assertTrue(theWorm.canMove(0));
	}
	
	@Test
	public void testMove_SingleCaseX() {
		theWorm.move(10);
		assertEquals(54.90033289,theWorm.getX(),EPS);
	}
	

	@Test
	public void testMove_SingleCaseY() {
		theWorm.move(10);
		assertEquals(50.99334665,theWorm.getY(),EPS);
	}
	
	@Test
	public void testMove_SingleCaseActionPoints() {
		theWorm.move(10);
		assertEquals(538,theWorm.getActionPoints(),EPS);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testMove_NegativeSteps() {
		theWorm.move(-10);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testMove_TooFar() {
		theWorm.move(10000);
	}
	
	@Test
	public void testCalculateMoveCost_SingleCase() {
		assertEquals(18,theWorm.calculateMoveCost(10),EPS);
	}
	
	@Test
	public void testCanTurn_TrueCase() {
		assertTrue(theWorm.canTurn(1));
	}
	
	@Test
	public void testCanTurn_TooMuch() {
		assertFalse(theWorm.canTurn(10000));
	}
	
	@Test
	public void testTurn_SingleCaseDirection() {
		theWorm.turn(0.3);
		assertEquals(0.5,theWorm.getDirection(),EPS);
	}
	
	@Test
	public void testTurn_SingleCaseActionPoints() {
		theWorm.turn(0.3);
		assertEquals(553,theWorm.getActionPoints(),EPS);
	}
	
	@Test
	public void testCalculateTurnCost_SingleCase() {
		assertEquals(3,theWorm.calculateTurnCost(0.3));
	}
	
	@Test
	public void testRestrictDirection_SingleCase() {
		assertEquals(Math.PI/2.0, Worm.restrictDirection(Math.PI/2.0),EPS);
	}
	
	@Test
	public void testRestrictDirection_TooBig() {
		assertEquals(Math.PI/2.0, (Worm.restrictDirection(9.0 * Math.PI / 2.0)), EPS);
	}
	
	@Test
	public void testRestrictDirection_TooSmall() {
		assertEquals(Math.PI/2.0, (Worm.restrictDirection(7.0 * - Math.PI / 2.0)), EPS);
	}
	
	@Test
	public void testRestrictDirection_LowerBound() {
		assertEquals(0,Worm.restrictDirection(0),EPS);
	}
	
	@Test
	public void testRestrictDirection_UpperBound() {
		assertEquals(0,Worm.restrictDirection(Math.PI * 2),EPS);
	}
	
	@Test
	public void testJump_SingleCaseX() {
		theWorm.jump();
		//actionpoints= 556
		//mass = 556.0618997
		//force = 8233.104429
		//v0 = 7.403046705
		//distance = 2.176289701
		assertEquals(52.176289701,theWorm.getX(),EPS);
	}
	
	@Test
	public void testJump_SingleCaseY() {
		theWorm.jump();
		assertEquals(50,theWorm.getY(),EPS);
	}
	
	@Test
	public void testJump_SingleCaseActionPoints() {
		theWorm.jump();
		assertEquals(0,theWorm.getActionPoints());
	}
	
	@Test
	public void testGetJumpTime_SingleCase() {
		assertEquals(0.2999512239, theWorm.getJumpTime(),EPS);
	}
	
	@Test (expected = ArithmeticException.class)
	public void testGetJumpTime_JumpStraight() {
		Worm theWorm2 = new Worm(50, 50, (Math.PI)/2.0, 0.5, "TheWormTwo");
		theWorm2.getJumpTime();
	}
	
	@Test (expected = UnsupportedOperationException.class)
	public void testGetJumpTime_ZeroActionPoints() {
		theWorm.setActionPoints(0);
		theWorm.getJumpTime();
	}
	
	@Test (expected = UnsupportedOperationException.class)
	public void testGetJumpTime_JumpDown() {
		Worm theWorm2 = new Worm(50, 50, 5, 0.5, "TheWormTwo");
		theWorm2.getJumpTime();
	}
	
	@Test
	public void testGetJumpStep_SingleCase() {
		double[] expected = {50.870657,50.105883};
		assertArrayEquals(expected, theWorm.getJumpStep(0.12), EPS);
	}
	
	@Test
	public void testGetJumpStep_LowerBound() {
		double[] expected = {50.0, 50.0};
		assertArrayEquals(expected, theWorm.getJumpStep(0), EPS);
	}
	
	@Test
	public void testGetJumpStep_UpperBound() {
		double[] expected = {52.176289701, 50.0};
		assertArrayEquals(expected, theWorm.getJumpStep(theWorm.getJumpTime()), EPS);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testGetJumpStep_NegativeT() {
		theWorm.getJumpStep(-10);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testGetJumpStep_TooLargeT() {
		theWorm.getJumpStep(10);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testGetJumpStep_IsNaN() {
		theWorm.getJumpStep(Double.NaN);
	}
	
}

